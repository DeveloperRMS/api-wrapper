export const CstApi = {
  UrlAPIBase: '/api/v1/',
  LocalDevUrlBase: 'http://localhost:3000',
  DevUrlBase: 'https://jwt.naruby.local',
  ProdUrlBase: '',
}


export const ApiLocatie = {
  LocalDev: 'LocalDev',
  Development: 'Development',
  Productie: 'Productie',
}

export const CstOmgevingInGebruik = ApiLocatie.Development

