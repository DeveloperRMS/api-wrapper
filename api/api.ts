import fs from 'fs'
import https from 'https'
import axios, { AxiosError, AxiosHeaders, AxiosRequestConfig, AxiosResponse } from 'axios'

import { CstApi, ApiLocatie, CstOmgevingInGebruik } from '../Cst'
import Client from './clientType'

export type ApiFout = {
  responseStatus: number | null,
  foutmelding: string,
  foutbericht?: string[]
}
// indien CA cert voorzien --> toevoegen als vertrouwd
function SelfSignedCA() {
  try {
    const caCrt = fs.readFileSync('./CA.crt')
    const httpsAgent = new https.Agent({ ca: caCrt, keepAlive: false })
    return httpsAgent
  } catch (err) {
    return new https.Agent({ keepAlive: false })
  }
}
// Bepaal de basis url aan de han van de constante CstOmgevingInGebruik (zie Cst.js)
function BepaalBasisUrl() {
  switch (CstOmgevingInGebruik) {
    case ApiLocatie.LocalDev:
      axios.defaults.baseURL = CstApi.LocalDevUrlBase + CstApi.UrlAPIBase
      break
    case ApiLocatie.Development:
      axios.defaults.baseURL = CstApi.DevUrlBase + CstApi.UrlAPIBase
      break
    case ApiLocatie.Productie:
      axios.defaults.baseURL = CstApi.ProdUrlBase + CstApi.UrlAPIBase
      break
    default:
      throw new Error(`Kan api basis url niet bepalen voor omgeving ${CstOmgevingInGebruik}`)
  }
}
// config voor Axios
// eventueel self signed CA toevoegen als vertrouwd
// eventueel Bearer token toevoegen in Authorization header
function configAxios(token?: string): AxiosRequestConfig {
  BepaalBasisUrl()
  const httpsAgent = SelfSignedCA()
  const headers: AxiosHeaders = new AxiosHeaders()
  if (token) {
    headers.Authorization = 'Bearer ' + token
  }
  const config: AxiosRequestConfig = { httpsAgent, headers }
  return config
}


function AntwoordVerwerking(response: AxiosResponse, url: string) {
  // antwoord met data
  if (response.status === 200) return response.data
  // leeg antwoord
  if (response.status === 201) return null

  // onvoorzien status die geen fout is
  const fout: ApiFout = {
    responseStatus: response.status,
    foutmelding: `API: ${response.status} ${response.data} - api url = ${axios.defaults.baseURL}${url}`,
  }
  console.debug(`Api AntwoordVerwerking, onvoorzien status die geen fout is:  ${response.status}: ${fout.foutmelding}`)
  throw new Error(JSON.stringify(fout))
}

function FoutVerwerking(axiosFout: AxiosError, url: string) {
  const { response } = axiosFout

  if (!response) {
    const fout: ApiFout = {
      responseStatus: null,
      foutmelding: `Geen antwoord: ${axiosFout.message} - api url = ${axios.defaults.baseURL}${url}`,
    }
    console.debug(`Api FoutVerwerking, geen antwoord: ${fout.foutmelding}`)
    throw new Error(JSON.stringify(fout))
  }

  if (response.status === 401 || response.status === 403) {
    //  Unauthorized / Forbidden
    const fout: ApiFout = {
      responseStatus: response.status,
      foutmelding: `Je hebt geen toegang om deze applicatie te gebruiken.Foutcode: ${response.status}`,
    }
    console.debug(`Api FoutVerwerking, status ${response.status}: ${fout.foutmelding}`)
    throw new Error(JSON.stringify(fout))
  }

  if (response.status === 400) {
    // Bad Request --> foutmeldingen van api uit antwoord halen zodat ze kunnen getoond worden
    const { data } = response
    const foutmeldingen = Array.isArray(data) ? data.reduce((acc, current) => acc + current) : data
    const fout: ApiFout = {
      responseStatus: response.status,
      foutmelding: 'Bad request',
      foutbericht: foutmeldingen,
    }
    console.debug(`Api FoutVerwerking: status 400: ${foutmeldingen} - api url = ${axios.defaults.baseURL}${url}`)
    throw new Error(JSON.stringify(fout))
  }

  // niet-verwerkte fout
  const fout: ApiFout = {
    responseStatus: response.status,
    foutmelding: `Fout ${response.status}: ${axiosFout.message} - api url = ${axios.defaults.baseURL}${url}`,
  }
  console.debug(`Api FoutVerwerking, niet verwerkte fout: ${response.status}: ${fout.foutmelding}`)
  return Promise.reject(new Error(JSON.stringify(fout)))
}


export async function AxiosGet(url: string, token?: string) {
  try {
    const result = await axios.get(url, configAxios(token))
    return AntwoordVerwerking(result, url)
  } catch (error) {
    const axiosFout = error as AxiosError
    return FoutVerwerking(axiosFout, url)
  }
}

export async function AxiosPost(postData: {}, url: string, token?: string): Promise<any> {
  try {
    const result = await axios.post(url, postData, configAxios(token))
    return AntwoordVerwerking(result, url)
  } catch (error) {
    const axiosFout = error as AxiosError
    return FoutVerwerking(axiosFout, url)
  }
}

export async function AxiosGetJWT(url: string, authUrl: string, token?: string):
  Promise<{ response: any, accessToken: string }> {
  const clientId = process.env.clientId
  if (!clientId) throw new Error('Geen clientId')
  const clientSecret = process.env.clientSecret
  if (!clientSecret) throw new Error('Geen clientSecret')

  const client: Client = { clientId, clientSecret }
  const accessToken = token ?? await AxiosPost(client, authUrl)

  const response = await AxiosGet(url, accessToken)
  return { response, accessToken }
}
